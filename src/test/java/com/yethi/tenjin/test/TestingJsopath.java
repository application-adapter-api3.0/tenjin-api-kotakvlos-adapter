package com.yethi.tenjin.test;

import org.json.JSONArray;

import com.google.gson.JsonElement;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class TestingJsopath {
	
	
	public static void main(String...strings ) {
		
		String jsonString = "{\r\n"
				+ "   \"OTPGeneration\":{\r\n"
				+ "      \"RequestBody\":{\r\n"
				+ "         \"OTPGenerationRequestBody\":{\r\n"
				+ "            \"requestId\":\"16Mvk1cmjvcgokilf2f2oo\",\r\n"
				+ "            \"email\":\"Surya.S@rblbank.com\",\r\n"
				+ "            \"mobileno\":\"8431544404\",\r\n"
				+ "            \"channelId\":\"EBAUTH\",\r\n"
				+ "            \"deliveryFlag\":\"S\",\r\n"
				+ "            \"serviceType\":\"S\",\r\n"
				+ "            \"customerName\":\"john\",\r\n"
				+ "            \"optionalField1\":\"Jo\",\r\n"
				+ "            \"optionalField2\":\"\",\r\n"
				+ "            \"optionalField3\":\"\",\r\n"
				+ "            \"optionalField4\":\"\",\r\n"
				+ "            \"optionalField5\":\"\",\r\n"
				+ "            \"TransactionRefID\":\"q12356789012\"\r\n"
				+ "         }\r\n"
				+ "      },\r\n"
				+ "      \"RequestHeader\":{\r\n"
				+ "         \"Security\":{\r\n"
				+ "            \"Token\":{\r\n"
				+ "               \"MessageHashKey\":\"\",\r\n"
				+ "               \"MessageIndex\":\"\",\r\n"
				+ "               \"PasswordToken\":{\r\n"
				+ "                  \"Password\":\"\",\r\n"
				+ "                  \"UserId\":\"101220914\"\r\n"
				+ "               },\r\n"
				+ "               \"Certificate\":\"\"\r\n"
				+ "            }\r\n"
				+ "         },\r\n"
				+ "         \"AdditionalInfo\":{\r\n"
				+ "            \"SessionId\":\"\",\r\n"
				+ "            \"JourneyId\":\"\",\r\n"
				+ "            \"LanguageId\":\"\",\r\n"
				+ "            \"SVersion\":\"\"\r\n"
				+ "         },\r\n"
				+ "         \"DeviceInfo\":{\r\n"
				+ "            \"DeviceName\":\"\",\r\n"
				+ "            \"DeviceVersion\":\"\",\r\n"
				+ "            \"DeviceID\":\"\",\r\n"
				+ "            \"DeviceOS\":\"\",\r\n"
				+ "            \"AppVersion\":\"\",\r\n"
				+ "            \"DeviceFormat\":\"\",\r\n"
				+ "            \"DeviceFamily\":\"\",\r\n"
				+ "            \"DeviceType\":\"\",\r\n"
				+ "            \"DeviceIp\":\"\",\r\n"
				+ "            \"DeviceIMEI\":\"\"\r\n"
				+ "         },\r\n"
				+ "         \"RequestMessageInfo\":{\r\n"
				+ "            \"MessageDateTime\":\"2022-02-18T12:48:49.034\",\r\n"
				+ "            \"BankId\":\"01\",\r\n"
				+ "            \"TimeZone\":\"GMT+05:00\"\r\n"
				+ "         },\r\n"
				+ "         \"MessageKey\":{\r\n"
				+ "            \"ChannelId\":\"BWY\",\r\n"
				+ "            \"RequestUUID\":\"1645168729048\",\r\n"
				+ "            \"ServiceRequestId\":\"GENOTP\",\r\n"
				+ "            \"ServiceRequestVersion\":\"1\"\r\n"
				+ "         }\r\n"
				+ "      }\r\n"
				+ "   }\r\n"
				+ "}";
		
	    DocumentContext docCtx = JsonPath.parse(jsonString);
	    JsonPath jsonPath = JsonPath.compile("$.OTPGeneration.RequestBody.OTPGenerationRequestBody.mobileno");
	    Object val1=docCtx.read(jsonPath);
	    System.out.println(val1);
		
	}
	

}
