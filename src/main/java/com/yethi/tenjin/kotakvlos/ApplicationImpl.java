package com.yethi.tenjin.kotakvlos;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.yethi.tenjin.api.models.ValidationResult;
import com.yethi.tenjin.api.models.Validparam;
import com.yethi.tenjin.executor.ActionResult;
import com.yethi.tenjin.sdk.api.adapter.ApiInterceptor;
import com.yethi.tenjin.sdk.api.adapter.DefaultInterceptor;
import com.yethi.tenjin.sdk.api.adapter.models.ClientRequest;
import com.yethi.tenjin.sdk.api.adapter.models.ClientResponse;
import com.yethi.tenjin.xmlutils.SampleCheck;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApplicationImpl extends DefaultInterceptor implements ApiInterceptor {

	Map<String, String> namespaces = new HashMap<String, String>();
	static List<Validparam>parameter=new ArrayList<Validparam>();
	List<String> outputlabel = new ArrayList<String>();
	static Map<String, String> value = new HashMap<String, String>();
	String encryptionKey = "";

	public void preRequest(ClientRequest request)
			throws ParserConfigurationException, TransformerException, IOException {
		String payload = null;
		log.info("Enter preRequest to xml data");
		//String xml = request.getPayload();
		String xml	= new SampleCheck().convertJsonToXml(request.getPayload());
		//String xml = new JsonToXmlConverter().convrtJsonToXml(request.getApi().getApiOperation().getRequestRepresentation().getRepresentationDescription(),request.getPayload());
		encryptionKey = request.getApi().getEncryptionKey();
		try {
			payload = CryptoUtils.encrypt(xml, encryptionKey);
			request.setPayload(payload);
			log.info("Converted to xml data");
			log.info("------------------------");
			log.info(payload);
			log.info("------------------------");
		} catch (Exception e) {
			log.error("Could Not build the request in ApplicationImpl Api");
			throw new IOException("Could Not build the request in ApplicationImpl Api");
		}
	}

	@Override
	public void postResponse(ClientResponse response) {
		String xmlResponseData = null;
		try {
			log.info("only for xml api");
			xmlResponseData = CryptoUtils.decrypt(response.getResponseData().toString(), encryptionKey);
			response.setResponseData(xmlResponseData);
			log.info("------------------------");
			log.info(xmlResponseData);
			log.info("------------------------");
			response.setReponseDateLableAndValue(this.labelFinderforxml(xmlResponseData));
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*added to find the label JSONformt*/
		
		/*--->setting validation result<---*/
		if (response.getValidationResult() != null) {
            response.getReponseDateLableAndValue();
            List<ValidationResult> valid = response.getValidationResult();
            List<Validparam>validparams=response.getParamValue();
            for (int i = 0; i < valid.size(); i++) {
                ValidationResult v = valid.get(i);
                
				for (int k = 0; k < validparams.size(); k++) {
					Validparam validparam = validparams.get(k);
					// if(validparam.getFeidname().contains(v.getField()))
					if (v.getPage().equals(validparam.getPage()) && v.getField().equals(validparam.getFeidname())) {
						// check for the page....
						validparam.getValue();
						v.setActualValue(validparam.getValue());
					}
				}
                try {
                if (v.getActualValue().equals(v.getExpectedValue())) {
                    v.setStatus(ActionResult.S);
                } else {
                    v.setStatus(ActionResult.F);
                }
                }
                catch (NullPointerException e) {
                    log.info("could not set value because of {}",e);
                }
           }
        }

		int status = response.getStatus();
		if (status == 201||status==200) {
			response.setResult(ActionResult.S);
			response.setRemarks("PASS");
			
			try {
				Map<String, String> msg = new HashMap<String, String>();
				msg = response.getReponseDateLableAndValue();
				msg.get("Msg").equalsIgnoreCase("please");
				response.setResult(ActionResult.F);
				response.setRemarks(msg.get("Msg"));
			} catch (Exception e) {
				response.setResult(ActionResult.S);
				response.setRemarks("PASS");
			}
		} else
			response.setResult(ActionResult.F);

	}

	public Map<String, String> labelFinderforxml(String file)
			throws ParserConfigurationException, SAXException, IOException, TransformerException {
		DocumentBuilderFactory docbuildfac = DocumentBuilderFactory.newInstance();

		// we are creating an object of builder to parse
		// the xml file.
		DocumentBuilder db = docbuildfac.newDocumentBuilder();
		Document doc = null;
		doc = db.parse(new InputSource(new StringReader(file)));
		System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

		if (doc.getDocumentElement().hasChildNodes()) {
			NodeList nodeList = doc.getDocumentElement().getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node childNode = nodeList.item(i);
				if (childNode.getNodeType() == Node.ELEMENT_NODE) {
					doSomething(childNode);
				}
			}
		}
		return value;
	}

	public static void doSomething(Node node) {
		Element element = (Element) node;
		try {
				parameter.add(new Validparam(node.getParentNode().getNodeName(), node.getNodeName(),element.getFirstChild().getNodeValue()));
				if(!value.containsKey(node.getNodeName())) {
				value.put(node.getNodeName(), element.getFirstChild().getNodeValue());
				}
				else {
					for(int i=2;i<=10;i++) {
						if(value.containsKey(node.getNodeName())) {
							
						String dup=node.getNodeName()+" "+"0"+i;
						if(!value.containsKey(dup))
						{
							value.put(dup, element.getFirstChild().getNodeValue());
							break;
						}
						}
					}
				}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node currentNode = nodeList.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				doSomething(currentNode);
			}
		}
	}
	
	public Map<String, String> labelForJson(String jsonresponse) {
		JSONObject obj = new JSONObject(jsonresponse);

		Set<String> jsons = obj.keySet();
		for (String json : jsons) {
			value.put(json, obj.getString(json));

		}
		return value;
	}
	
	public static String filterDecode(String url){

		   url = url.replaceAll("&amp;", "&")
		     .replaceAll("&lt;", "<")
		     .replaceAll("&gt;", ">")
		     .replaceAll("&apos;", "\'")
		     .replaceAll("&quot;", "\"")
		     .replaceAll("&nbsp;", " ")
		     .replaceAll("&copy;", "@")
		     .replaceAll("&reg;", "?")
		     .replaceAll("&quot;", "<")
		     .replaceAll("&quot;:", ">");
		   return url;
	 }
	
	public void validateField(ClientResponse response) {
		if (response.getValidationResult() != null) {
            response.getReponseDateLableAndValue();
            List<ValidationResult> valid = response.getValidationResult();
            List<Validparam>validparams=response.getParamValue();
            for (int i = 0; i < valid.size(); i++) {
                ValidationResult v = valid.get(i);
                
				for (int k = 0; k < validparams.size(); k++) {
					Validparam validparam = validparams.get(k);
					if (v.getPage().equals(validparam.getPage()) && v.getField().equals(validparam.getFeidname())) {
						validparam.getValue();
						v.setActualValue(validparam.getValue());
						break;
					}
				}
                try {
                if (v.getActualValue().equals(v.getExpectedValue())) {
                    v.setStatus(ActionResult.S);
                } else {
                    v.setStatus(ActionResult.F);
                    response.setResult(ActionResult.F);
        			response.setRemarks("Failed due to validation fail");
                } } catch (NullPointerException e) {
                    log.info("could not set value because of {}",e);
                }
           }
        }
	}
}