package com.yethi.tenjin.kotakvlos;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.yethi.tenjin.executor.ActionResult;
import com.yethi.tenjin.sdk.api.adapter.models.ClientRequest;
import com.yethi.tenjin.sdk.api.adapter.models.ClientResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResponseValidateImpl {

	String errorMessage = null;
	int expectedResponseCode;
	String expectedResponse;
	JsonObject expectedJsonResponse;

	public void clientRequest(ClientRequest request) {
		this.expectedResponseCode = request.getApi().getApiOperation().getResponseRepresentation().getResponseCode();
		this.expectedResponse = request.getApi().getApiOperation().getResponseRepresentation()
				.getRepresentationDescription().toString();
		expectedJsonResponse = new Gson().fromJson(expectedResponse, JsonObject.class);

	}

	void responseWithStatus(ClientResponse response, JsonObject decryptedJsonResponse) {
		this.errorMessage = decryptedJsonResponse.get("error").toString();
		if (expectedResponseCode == response.getStatus()
				&& decryptedJsonResponse.get("status").equals(expectedJsonResponse.get("status"))) {

			response.setResult(ActionResult.S);
			response.setRemarks(errorMessage);
			response.setResponseData(decryptedJsonResponse.toString());
			log.info(decryptedJsonResponse.toString());
		} else {
			response.setResult(ActionResult.F);
			response.setRemarks(errorMessage);
			response.setResponseData(decryptedJsonResponse.toString());
		}
	}

	void responseWithStatusCode(ClientResponse response, JsonObject decryptedJsonResponse) {
		this.errorMessage = "HTTP Status Code:" + decryptedJsonResponse.get("statusCode").toString();
		if (expectedResponseCode == response.getStatus()
				&& decryptedJsonResponse.get("statusCode").equals(expectedJsonResponse.get("statusCode"))) {
		
			response.setResult(ActionResult.S);
			response.setRemarks(errorMessage);
			response.setResponseData(decryptedJsonResponse.toString());
			log.info(decryptedJsonResponse.toString());
		} else {
			response.setResult(ActionResult.F);
			response.setRemarks(errorMessage);
			response.setResponseData(decryptedJsonResponse.toString());
		}
	}
}
