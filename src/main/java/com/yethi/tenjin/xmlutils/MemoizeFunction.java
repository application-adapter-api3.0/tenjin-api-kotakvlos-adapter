package com.yethi.tenjin.xmlutils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

public abstract class MemoizeFunction<F, T> implements Function<F, T> {
	private final Map<F, T> cache = new LinkedHashMap<>();
	 public abstract T calc(final F n);
//	@Override
//	public T apply(F t) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	public T apply(final F key) {
        cache.putIfAbsent(key, calc(key));
        return cache.get(key);
    }

}
