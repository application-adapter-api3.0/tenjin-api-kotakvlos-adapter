package com.yethi.tenjin.xmlutils;

public interface PredicateIndexed<T> {
	
	boolean test(int index, T arg);

}
